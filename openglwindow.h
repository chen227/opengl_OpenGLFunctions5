﻿#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include <QtGui/QWindow>
#include <QtGui/QOpenGLFunctions>
#include <QtGui/QOpenGLExtraFunctions>

QT_BEGIN_NAMESPACE
class QPainter;
class QOpenGLContext;
class QOpenGLPaintDevice;
QT_END_NAMESPACE

class Openglwindow : public QWindow, protected QOpenGLExtraFunctions
{
    Q_OBJECT
public:
    explicit Openglwindow(QWindow *parent=0);
    ~Openglwindow();

    //基于QPainter渲染
    virtual void render(QPainter *parent);
    //基于opengl渲染
    virtual void render();
    //初始化
    virtual void initialize();
    //设置动画
    void setAnimating(bool animating);

public slots:
    //手动刷新一次
    void renderLater();
    //立刻渲染
    void renderNow();

protected:
    //监听事件
    bool event(QEvent *event) override;
    //监听暴露事件
    void exposeEvent(QExposeEvent *event) override;

private:
    //动画状态位
    bool m_animating;

    //OpenGL 上下文
    QOpenGLContext *m_context;
    //OpenGL画笔设备，能使用QPainter在上下文里画东西
    QOpenGLPaintDevice *m_device;
};

#endif // OPENGLWINDOW_H
